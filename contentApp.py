import webApp

class contentApp(webApp.webApp):
    def __init__(self, port, hostname):
        self.resources = {"Alex": "<html><body><h1>Hola Alex</h1></html></body>",
                          "Juan": "<html><body><h1>Hola Juan</h1></html></body>",
                          "Pedro": "<html><body><h1>Hola Pedro</h1></html></body>",
                          "Main": "<html><body><h1>Hola Mundo</h1></html></body>",
                         }
        super().__init__(port, hostname)

    def parse(self, request):
        # parseo la petición extrayendo la info
        try:
            if request.split(" ")[1] != "/":
                resource = request.split(" ")[1].split("/")[1]
            else:
                resource = "Main"
        except IndexError:
            resource = None
        return resource

    def process(self, parsed_request):
        # Procesa los datos de la petición. Devuelve el código htpp de la respuesta y una pag HTML
        if parsed_request in self.resources.keys():
            code = "200 OK"
            html = self.resources[parsed_request]
        else:
            code = "404 Not Found"
            html = "<html><body><h1>Recurso no encontrado</h1></html></body>"
        return code, html

if __name__ == '__main__':
    app = contentApp(1234, 'localhost')