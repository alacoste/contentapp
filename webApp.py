import socket

class webApp:
    def __init__(self, port, hostname):
        self.port = port
        self.hostname = hostname
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(5)  #.. 5 conexiones simultáneas máximo TCP
        try:
            while True:
                print("Esperando conexión...")
                clientsocket, addr = self.socket.accept()
                print("Conexión establecida con: ", addr)
                request = clientsocket.recv(2048)
                request = request.decode('utf-8')
                # Manejar la petición
                parsed_req = self.parse(request)

                # Proceso la petición y genero la respuesta
                code, html = self.process(parsed_req)

                # Enviar respuesta al cliente
                respuesta = "HTTP/1.1 " + code + "\r\n\r\n" + html + "\r\n"
                clientsocket.send(respuesta.encode('utf-8'))
                clientsocket.close()
        except KeyboardInterrupt:
            print("closing server...")
            self.socket.close()

    def parse(self, request):
        # parseo la petición extrayendo la info
        print("Parse: no hay nada que parsear ahora mismo")
        return None

    def process(self, parsed_request):
        # Procesa los datos de la petición. Devuelve el código htpp de la respuesta y una pag HTML
        print("Process: no hay nada que procesar ahora mismo")
        return "200 OK", "<html><body><h1>Hola mundo</h1></html></body>"